import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter your name: ");
        String name = input.nextLine();
        System.out.printf("Hello %s!\nPlease, enter a sequence of integers:\n", name);
        String sequenceOfIntegers = input.nextLine();
        String[] sequenceOfNumbers = sequenceOfIntegers.split(" ");
        int sumOfNumbers = 0, productOfNumbers = 1;
        for (int i = 0; i < sequenceOfNumbers.length; i++){
            sumOfNumbers += Integer.parseInt(sequenceOfNumbers[i]);
            productOfNumbers *= Integer.parseInt(sequenceOfNumbers[i]);
        }
        System.out.printf("The sum of the sequence of numbers is %d. \n" +
                "The product of a sequence of numbers is %d.", sumOfNumbers, productOfNumbers);
    }
}
